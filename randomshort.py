#Servicios Telematicos (2022-2023)
#Sara Gomez Civantos - Grado Ingenieria Telematica
#Minipractica 1

import webapp
import random
import string
from urllib.parse import unquote


#Formulario POST
form = """
        <form action="/" method="POST">
            URL a acortar: <input type="text" name="url">
            <input type="submit" value="Submit!">
        </form> 
        """


class randomShort(webapp.webApp):
    """Aplicacion web simple acortadora de URLs"""

    urls = {} #Creo diccionario para almacenar urls

    def parse(self, request): #Analizo lo que pide
        """Analizado las solicitudes y troceo en 3 elementos, separando los 2 primeros"""

        request = request.decode()
        metodo = request.split(' ', 2)[0] # 2->devuelve en forma de 3 elementos
        recurso = request.split(' ', 2)[1]
        cuerpo = unquote(request.split('\r\n\r\n', 1)[1])
        return metodo, recurso, cuerpo

    def process(self, analyzed):
        """Analizo las solicitudes del usuario"""

        metodo, recurso, cuerpo = analyzed

        if metodo =="GET": #Si recurso es / -> devuelvo pagina html con formulario
            if recurso == "/":
                http = "200 OK"
                html = "<html><body>" + form + "<h1>Lista de URLs acortadas:</h1><br> " + str(self.urls) + "<br><br>" \
                       + "<h1>Lista de URLs :</h1><br> " + str(self.urls) + "<html><body>"
            elif recurso in self.urls: #Si ya tenemos guardado ese recurso redirecciono con url acortada
                http = "200 OK"
                html = "<html><body> Redireccionando a: " + str(self.urls[recurso]) + "..." \
                           + '<meta http-equiv="refresh" content="2;url=' + (self.urls[recurso]) + '">' + "</body></html>" + "</body></html>"
            else: #Si el recurso es erroneo o no esta
                http = "404 Not Found"
                html = "<html><body>El recurso que ha introducido no se encuentra.<body><html>"

        elif metodo == "POST":
            recurso_aux = analyzed[2].split("=")[1]
            if str(recurso_aux) == "/": # Si el campo del formulario es "/"
                http = " 200 OK"
                html = "<html><body>" + "<h1>Listado de URLs acortadas:</h1><br> " + str(self.urls) + "<br><br>" \
                       + "<h1>Lista de URLs:</h1><br>" + str(self.urls) + "</body></html>"
            elif not str(recurso_aux): # Si no hay nada guardado -> Redirecciono a localhost
                http = " 301 Moved Permanently"
                html = "<html><body> No se ha introducido ninguna URL" + "<br>" + "Se redirecionara a la pagina inicial... " \
                       + '<meta http-equiv="refresh" content="2;url=' + 'http://localhost:1234/' + '">' + "</body></html>"
            else:
                aux = recurso_aux
                if (not recurso_aux.startswith('https://')) and (not recurso_aux.startswith('http://')): #Si no empieza por http:// - https:// se lo añado
                    aux = "https://" + recurso_aux
                if aux not in self.urls.values(): #Si no la tengo guardada --> #Recursos aleatorios para urls acortadas
                    id = '/' + ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
                    self.urls[id] = aux
                    http = " 301 Moved Permanently"
                    html = "<html><body> Redireccionando a pagina de inicio... " \
                               + '<meta http-equiv="refresh" content="3;url=' + 'http://localhost:1234/' +  '">'+ "</body></html>"
                else:# Si la URL introducida ya está en mi lista de URLs conocidas -> Redirecciona a pagina inicial(localhost)
                    http = " 301 Moved Permanently"
                    html = "<html><body> La URL ya esta acortada. Se redireccionara a la pagina inicial..." \
                               + '<meta http-equiv="refresh" content="3;url=' + 'http://localhost:1234/' + '">' + "</body></html>"
        else: # Si el metodo no es un GET ni un POST
            http = "404 Not Found"
            html = "<html><body> El recurso solicitado no esta disponible <html><body>"

        return http, html


if __name__ == "__main__":
    app = randomShort('localhost', 1234)
